// PARAMETRIC PIPETTE HOLDER //
// 
// Description: Holders for "Gilson-style" single-channel micropipettes that can be 3D printed and attached to surfaces with adhesive. Number of pipettes and other parameters can also be varied.

// HOW TO USE THIS FILE //
// 1) Change the personalisable parameters below. This is pipette_qty, you can also change any other parameters e.g. to adjust for different brands of pipette that have different dimensions to Gilsons.
// 2) Render in OpenSCAD. By default all parts are rendered together, if you want to render individual parts you can comment out the ones you don't need but it is probably easier to just delete them in you laser cutter software software
// 3) Export an STL file and load directly into your 3D printer software.
// 6) Attach to the wall of your lab with adhesive e.g. Command strips.

// PERSONALISABLE PARAMETERS - PLEASE CHANGE THESE //
// Number of single-channel "Gilson-style" micropipettes that the stand can hold
pipette_qty = 6;


// OTHER PARAMETERS - CHANGE THESE WITH CAUTION //
// Overall width of stand 
stand_width = ((pipette_qty*30)+15);
// Depth of base, may need adjusting to match centre of gravity and avoid the stand falling over
stand_base = 100;
// Overall height of stand
stand_height = 260;
//Width of the cut-out to hold the micropipette, must be narrower than the micropipette
holder_width = 23;
holder_depth = 80;
holder_shelf_depth = 60;

// COMPONENT MODULES //


// Cut out holder for one single-channel "Gilson-style" micropipette
module holder(){
    resize([holder_width,holder_depth])cylinder(h=40, r1=holder_width/2, r2=holder_width/2, center=true);;
}

// PART MODULES //

// Set depth, height and width of slope to get magnet closer to tube
wedge_angle = atan(5);
wedge_d =  (10);
wedge_h = tan(wedge_angle)*wedge_d;
wedge_w = stand_width;

// Set depth, height and width of slope to get magnet closer to tube

module wedge(){
        rotate(a=[0,90,0])
            linear_extrude(height = wedge_w, center = false, convexity = 0, twist = 0)
            polygon(points=[[0,0],[wedge_h,0],[0,wedge_d]], paths=[[0,2,1]]);

}

module cut_template(){
    translate([0,0,-(stand_width+(wedge_h*0.5))])
    cube(stand_width);
}


module holder_shelf(){
difference(){
union(){
cube(size = [stand_width, (holder_shelf_depth),10], center = false);

}
for (i=[1:pipette_qty])  {
translate([(i*(stand_width/pipette_qty)-(stand_width/pipette_qty)/2),(holder_depth-29),0]) 
    holder();
}
}
}

module wedge_cut(){
    difference(){
        wedge();
        cut_template();
    }
}

// RUN MODULES //
holder_shelf();
wedge_cut();

// END //
