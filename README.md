# Parametric 3D-printed Pipette Holders for Wall-Mounting

[[_TOC_]]

## Summary

**Project Status:** Prototype, work in progress.

Holders for "Gilson-style" single-channel micropipettes that can be 3D printed and attached to surfaces with adhesive. Number of pipettes and other parameters can also be varied.

<img src="https://gitlab.com/open-bioeconomy-lab/open-lab-hardware/3d-pipette-holders/-/raw/main/images/side-view.jpg"  width="40%" > <img src="https://gitlab.com/open-bioeconomy-lab/open-lab-hardware/3d-pipette-holders/-/raw/main/images/top-view.jpg"  width="40%" >

<img src="https://gitlab.com/open-bioeconomy-lab/open-lab-hardware/3d-pipette-holders/-/raw/main/images/1-pipette-render.png"  width="40%" > <img src="https://gitlab.com/open-bioeconomy-lab/open-lab-hardware/3d-pipette-holders/-/raw/main/images/4-pipette-render.png"  width="40%" >

## Instructions for 3D Printing

There are pre-generated stl files in the [stl folder](./stl/) that hold 1-6 pipettes.

No supports are needed for this design but a brim is recommended to ereduce warping and improve adhesion to the bed. Print on the base, with the holder arms pointing upwards. We recommend a 10-20% infill.

It has been successfully printed in PLA with 0.2m layers and a two-pipette version takes around 2 hours to print. This could be improved with optimisation, please consider editing the file and contributing your improved version!

<img src="https://gitlab.com/open-bioeconomy-lab/open-lab-hardware/3d-pipette-holders/-/raw/main/images/1-pipette-render.png"  width="50%" > 

Once printed, apply adhesive or tape such as Command Strips and mount to a suitable surface in your lab.

## Making things better
I'm an OpenSCAD beginner so if you spot something that could be done better or more elegantly, please fix it!

To contribute new design improvements, documentation updates or parameters for different types of pipettes, raise an issue in the repository or email jcm80@cam.ac.uk.

All improvements and suggestions are welcome! Please follow our [Code of Conduct](./Code_of_Conduct.md) for all contributions and communications within the project.

## To use the design file

1.  Download OpenSCAD https://www.openscad.org/ and review a [basic OpenSCAD tutorial](https://all3dp.com/2/openscad-tutorial-for-beginners-5-easy-steps/) so you know where the **Render** and **Export as STL** buttons are.
2.  Open the relevant file in the [OpenSCAD folder](./openscad)
3.  Change the personalisable parameters: pipette_qty (number of single-channel "Gilson-style" micropipettes that the stand can hold). Other parameters can also be adjusted to fit different styles of pipettes. 
4. Click **Render** and then **Export as STL** ready for importing to your 3D printer slicing software.


## License
Designed in OpenSCAD by Jenny Molloy (jcm80@cam.ac.uk, University of Cambridge, UK). Pipette holder dimensions are based on an original design by Prince Samoh (Ashesi University, Ghana). Copyright Open Bioeconomy Lab 2023, licensed under the CERN OHL-P (https://ohwr.org/cern_ohl_p_v2.txt)

